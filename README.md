# ghillie-bot-tm

Speeds up Ghillieglade farming.

## Commands:

- __**/8 ggb**__ to toggle ON/OFF (Default ON)

## Info:

- Spawns you on the bridge facing Banyaka when entering Ghillieglade.
- Resets Ghillieglade when entering Velik's Sanctuary, but only if loot chest was opened in the previous entry.
- Spawns you at the Creation Workshop banker in Velika when leaving Ghillieglade.